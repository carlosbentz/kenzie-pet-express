﻿# Kenzie Pet

# Migrations

Para rodar as migrations, cole o seguinte comando:
`npx sequelize-cli db:migrate --models-path src/models --migrations-path src/migrations --config src/config/config.json`

# Rotas

Todas as rotas são protegidas, sendo necessário um token para acessálas, as rotas Post, put e delete animals, necessitam de um token de super usuário, ou seja, com "is_superuser" true.

### `POST /api/signup/`

Esta rota é para a criação de usuários.

RESPONSE STATUS -> HTTP 201 (created)

Body:

{
"username": "carlos",
"password": "123",
"is_superuser": true
}

Response:

{
"id": 4,
"username": "carlos",
"is_superuser": true
}

### `POST /api/login/`

Rota de login, retorna um token.

RESPONSE STATUS -> HTTP 201 (created)

Body:

{
"username": "carlos",
"password": "123",
}

Response:

{
"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImNhcmxvcyIsImlhdCI6MTYzNDgzNjM4NH0.mAFdh5Atne3jSgO_SGxPp6S8POXu109xK6qRLh6f9hg"
}

### `GET /api/animals/`

Esta rota retorna todos os animais cadastrados no banco.

RESPONSE STATUS -> HTTP 200 (ok)

Response:

    [
      {
        "id": 1,
        "name": "Bidu",
        "age": 1.0,
        "weight": 30.0,
        "sex": "macho",
        "group": {
          "id": 1,
          "name": "cao",
          "scientific_name": "canis familiaris"
        },
        "characteristic_set": [
          {
            "id": 1,
            "characteristic": "peludo"
          },
          {
            "id": 2,
            "characteristic": "medio porte"
          }
        ]
      },
      {
        "id": 2,
        "name": "Hanna",
        "age": 1.0,
        "weight": 20.0,
        "sex": "femea",
        "group": {
          "id": 2,
          "name": "gato",
          "scientific_name": "felis catus"
        },
        "characteristic_set": [
          {
            "id": 1,
            "characteristic": "peludo"
          },
          {
            "id": 3,
            "characteristic": "felino"
          }
        ]
      }
    ]

### `GET /api/animals/<int:animal_id>/`

Esta rota retorna as informações do animal com id igual ao passado na rota.

RESPONSE STATUS -> HTTP 200 (ok)

    {
      "id": 1,
      "name": "Bidu",
      "age": 1.0,
      "weight": 30.0,
      "sex": "macho",
      "group": {
        "id": 1,
        "name": "cao",
        "scientific_name": "canis familiaris"
      },
      "characteristic_set": [
        {
          "id": 1,
          "characteristic": "peludo"
        },
        {
          "id": 2,
          "characteristic": "medio porte"
        }
      ]
    }

### `POST /api/animals/`

Esta rota é para a criação de informações de animais.

RESPONSE STATUS -> HTTP 201 (created)

Body:

    {
      "name": "Bidu",
      "age": 1,
      "weight": 30,
      "sex": "macho",
      "group": {
        "name": "cao",
        "scientific_name": "canis familiaris"
      },
      "characteristic_set": [
        {
          "characteristic": "peludo"
        },
        {
          "characteristic": "medio porte"
        }
      ]
    }

Response:

    {
      "id": 1,
      "name": "Bidu",
      "age": 1.0,
      "weight": 30.0,
      "sex": "macho",
      "group": {
        "id": 1,
        "name": "cao",
        "scientific_name": "canis familiaris"
      },
      "characteristic_set": [
        {
          "id": 1,
          "characteristic": "peludo"
        },
        {
          "id": 2,
          "characteristic": "medio porte"
        }
      ]
    }

### `PUT /api/animals/<int:animal_id>/`

Body:

    {
      "name": "mima",
      "age": 1,
      "weight": 30,
      "sex": "femea",
      "group": {
        "name": "gato",
        "scientific_name": "gatus familiaris"
      },
      "characteristics": [
        {
          "name": "pelo curto"
        },
        {
          "name": "pikenor"
        }
      ]
    }

Response:

    {
      "id": 10,
      "name": "mima",
      "age": 1,
      "weight": 30,
      "sex": "femea",
      "group": {
        "id": 1,
        "name": "cao",
        "scientific_name": "canis familiaris"
      },
      "characteristics": [
        {
          "id": 1,
          "name": "pelo curto"
        },
        {
          "id": 2,
          "name": "pikenor"
        }
      ]
    }

### `DELETE /api/animals/<int:animal_id>/`

Rota para deletar as informações de um animal.

Não há conteúdo no retorno da requisição.

RESPONSE STATUS -> HTTP 204 (no content)
