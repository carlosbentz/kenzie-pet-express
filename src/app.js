const express = require("express");
const app = express();

const routes = require("./api/index");
const PORT = 3000;

const applyPassportStrategy = require("./api/loaders/passport");
const passport = require("passport");

app.use(express.json());
applyPassportStrategy(passport);

routes(app);

app.listen(PORT, () => {
  console.log(`App rodando na porta <http://localhost:${PORT}>`);
});
