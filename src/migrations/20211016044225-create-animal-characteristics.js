"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("AnimalCharacteristics", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      animalId: {
        type: Sequelize.INTEGER,
        unique: false,
        allowNull: false,
        references: {
          model: "Animals",
          key: "id",
        },
      },
      characteristicId: {
        type: Sequelize.INTEGER,
        unique: false,
        allowNull: false,
        references: { model: "Characteristics", key: "id" },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("AnimalCharacteristics");
  },
};
