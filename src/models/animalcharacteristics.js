"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class AnimalCharacteristics extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  AnimalCharacteristics.init(
    {
      animalId: DataTypes.INTEGER,
      characteristicId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "AnimalCharacteristics",
    }
  );
  return AnimalCharacteristics;
};
