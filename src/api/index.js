const animalRouter = require("./routers/animal");
const userRouter = require("./routers/user");

module.exports = (app) => {
  animalRouter(app);
  userRouter(app);

  //   app.get("/", (request, response) => {
  //     response.send("Hello World!");
  //   });
  // app.post("/", (request, response) => {
  //   const { name } = request.body;
  //   if (!name) {
  //     return response.status(400).send({ Error: "Missing name" });
  //   }
  //   return response.status(201).send(`Hello ${name}`);
  // });
  // app.get("/query_params/", (request, response) => {
  //   const data = request.query; // Objeto contento valoresponse passados
  //   response.send(data);
  // });
  // app.get("/url_params/:itemId(//d+)", (request, response) => {
  //   //(//d+) é um regexp indicando que apenas números serão aceitos
  //   const data = request.params; // Objeto contento valoresponse passados
  //   response.send(data);
  // });
};
