module.exports.validate = (req, res, next) => {
  const body = req.body;

  let errors = {};

  if (!body.username) {
    errors.username = "Missing field username";
  }

  if (!body.password) {
    errors.password = "Missing field password";
  }

  if (Object.keys(errors).length !== 0) {
    return res.status(400).send(errors);
  }

  next();
};

module.exports.isSuperUser = (req, res, next) => {
  const user = req.user;

  if (!user.is_superuser) {
    return res.status(403).send({ Error: "User is not a superuser" });
  }

  next();
};
