module.exports.validate = (req, res, next) => {
  const body = req.body;

  let errors = {};

  if (!body.name) {
    errors.name = "Missing field name";
  }

  if (!body.age) {
    errors.age = "Missing field age";
  }

  if (!body.weight) {
    errors.weight = "Missing field weight";
  }

  if (!body.sex) {
    errors.sex = "Missing field sex";
  }

  if (!body.group) {
    errors.group = "Missing field group";
  }

  if (
    body.group !== undefined &&
    (body.group.name == undefined || body.group.scientific_name == undefined)
  ) {
    errors.group = "Missing field group attributes";
  }

  if (!body.characteristics) {
    errors.characteristics = "Missing field characteristics";
  } else if (!Array.isArray(body.characteristics)) {
    errors.characteristics = "Field characteristics should be an array";
  }

  if (Object.keys(errors).length !== 0) {
    return res.status(400).send(errors);
  }

  next();
};
