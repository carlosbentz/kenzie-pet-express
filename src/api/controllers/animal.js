const { Animal, Group, Characteristic } = require("../../models");
const { animalSerializer } = require("../services/animal");

module.exports.list = async (req, res) => {
  // const animals = await Animal.findAll({
  //   attributes: {
  //     exclude: ["createdAt", "updatedAt"],
  //   },
  //   include: [
  //     { model: Group, attributes: { exclude: ["createdAt", "updatedAt"] } },
  //     {
  //       model: Characteristic,
  //       attributes: { exclude: ["createdAt", "updatedAt"] },
  //       through: { attributes: [] },
  //     },
  //   ],
  // });
  const animals = await Animal.findAll();
  serializedAnimals = [];
  for (let animal of animals) {
    serializedAnimals.push(await animalSerializer(animal));
  }

  res.json(serializedAnimals);

  res.json(animals);
};

module.exports.create = async (req, res) => {
  const group = await Group.findOrCreate({ where: req.body.group });

  let animal = await Animal.create(req.body);
  await animal.setGroup(group[0]);

  for (let characteristic of req.body.characteristics) {
    let char_instance = await Characteristic.findOrCreate({
      where: characteristic,
    });

    if (char_instance) {
      await animal.addCharacteristic(char_instance[0]);
    }
  }

  await animal.save();

  animal = await animalSerializer(animal);

  res.status(201).json(animal);
};

module.exports.retrieve = async (req, res) => {
  let animal = await Animal.findByPk(req.params.id);

  if (!animal) {
    return res.sendStatus(404);
  }

  animal = await animalSerializer(animal);

  res.json(animal);
};

module.exports.update = async (req, res) => {
  let animal = await Animal.findByPk(req.params.id);

  if (!animal) {
    return res.sendStatus(404);
  }

  await animal.update(req.body);

  const group = await Group.findOrCreate({ where: req.body.group });
  await animal.setGroup(group[0]);

  const characteristics = await animal.getCharacteristics();
  await animal.removeCharacteristics(characteristics);

  for (let characteristic of req.body.characteristics) {
    let new_characteristic = await Characteristic.findOrCreate({
      where: characteristic,
    });

    if (new_characteristic) {
      await animal.addCharacteristic(new_characteristic[0]);
    }
  }

  await animal.save();

  animal = await animalSerializer(animal);

  res.json(animal);
};

module.exports.destroy = async (req, res) => {
  const animal = await Animal.findByPk(req.params.id);

  if (!animal) {
    return res.sendStatus(404);
  }

  const characteristics = await animal.getCharacteristics();
  await animal.removeCharacteristics(characteristics);

  await animal.destroy();

  res.sendStatus(204);
};
