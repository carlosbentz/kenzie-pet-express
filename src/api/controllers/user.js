const { User } = require("../../models");
const { userSerializer } = require("../services/user");
const config = require("../../config/passport");
const jwt = require("jsonwebtoken");

module.exports.signup = async (req, res) => {
  userExists = await User.findOne({
    where: { username: req.body.username },
  });

  if (userExists) {
    userExists = await userSerializer(userExists);
    return res.status(200).json(userExists);
  }

  let user = new User(req.body);
  await user.hashPassword();
  await user.save();

  user = await userSerializer(user);

  res.status(201).json(user);
};

module.exports.login = async (req, res) => {
  let user = await User.findOne({
    where: { username: req.body.username },
  });

  if (!user) {
    return res.status(401).json({ error: "User not found" });
  }

  const validatePassword = await user.validatePassword(req.body.password);

  if (!validatePassword) {
    return res.status(401).json({ error: "Invalid Password" });
  }

  const token = jwt.sign({ username: user.username }, config.passport.secret);

  res.status(200).json({ token: token });
};
