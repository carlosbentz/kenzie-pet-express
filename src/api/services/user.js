module.exports.userSerializer = async (user) => {
  const { id, username, is_superuser } = user;

  return {
    id,
    username,
    is_superuser,
  };
};
