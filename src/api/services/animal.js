const { Group } = require("../../models");

module.exports.animalSerializer = async (animal) => {
  const { id, name, age, weight, sex } = animal;

  const animal_group = await Group.findOne({ where: animal });

  let group = {};

  if (animal_group) {
    group = {
      id: animal_group.id,
      name: animal_group.name,
      scientific_name: animal_group.scientific_name,
    };
  }

  const characteristics = await animal.getCharacteristics();
  let new_characteristics = [];

  if (characteristics) {
    for (let characteristic of characteristics) {
      new_characteristics.push({
        id: characteristic.id,
        name: characteristic.name,
      });
    }
  }

  return {
    id,
    name,
    age,
    weight,
    sex,
    group,
    characteristics: new_characteristics,
  };
};
