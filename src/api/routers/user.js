const { Router } = require("express");

const router = new Router();

const middlewares = require("../middlewares/user");

module.exports = (app) => {
  const { signup, login } = require("../controllers/user");

  router.post("/signup", middlewares.validate, signup);
  router.post("/login", middlewares.validate, login);

  app.use("/api", router);
};
