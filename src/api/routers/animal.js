const { Router } = require("express");
const passport = require("passport");
const router = new Router();

const animalMiddlewares = require("../middlewares/animal");
const userMiddlewares = require("../middlewares/user");

module.exports = (app) => {
  const {
    list,
    create,
    retrieve,
    update,
    destroy,
  } = require("../controllers/animal");

  router.get(
    "/animals",
    passport.authenticate("jwt", { session: false }),
    list
  );
  router.post(
    "/animals",
    passport.authenticate("jwt", { session: false }),
    userMiddlewares.isSuperUser,
    animalMiddlewares.validate,
    create
  );
  router.get(
    "/animals/:id(\\d+)",
    passport.authenticate("jwt", { session: false }),
    retrieve
  );
  router.put(
    "/animals/:id(\\d+)",
    passport.authenticate("jwt", { session: false }),
    userMiddlewares.isSuperUser,
    animalMiddlewares.validate,
    update
  );
  router.delete(
    "/animals/:id(\\d+)",
    passport.authenticate("jwt", { session: false }),
    userMiddlewares.isSuperUser,
    destroy
  );

  app.use("/api", router);
};
